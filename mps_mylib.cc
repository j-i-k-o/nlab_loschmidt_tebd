#include "itensor/all.h"
#include "itensor/mps/siteset.h"
#include <stdio.h>
#include <fstream>
#include <cmath>
#include "t_dmrg.h"

using namespace itensor;
using namespace myLib;

int main(){

	std::ofstream ofs("result_tebd");
	ofs << "#tf res\n";

	double vJ = -1;
	double vh = 0;
	double vG = +1;
	double A1 = 2;
	double B0 = 6;
	double B1 = 4;

	double dt = 0.01;

	for(size_t size : {1000}){
		for(double tf=0.1; tf<10; tf+=0.1){

			myLib::myvector<std::pair<double, double>> coeffs(size);
			for(auto& coeff : coeffs){
				coeff.first = 1;
				coeff.second = 0;
			}
			myLib::myvector<std::pair<double, double>> coeffs_m(size);
			for(auto& coeff : coeffs_m){
				coeff.first = 0;
				coeff.second = 1;
			}
			//initial state => ground state of -sx
			myLib::myMPS mps(coeffs);
			myLib::myMPS init_mps(coeffs, mps.site_c());
			myLib::myMPS init_mps_rev(coeffs_m, mps.site_c());

			//initialize each coefficient
			myvector<double> J = std::vector<double>(size-1, vJ/2.);
			myvector<double> h = std::vector<double>(size, vh/2.);
			myvector<double> G = std::vector<double>(size, vG/2.);
			double t1 = A1/100;
			double t2 = tf-t1;

			auto A = [=](double t){
				if(0<=t && t<t1){
					return A1*(t/t1);
				}
				else if(t1<=t && t<t2){
					return A1;
				}
				else{
					return A1*(tf-t)/(tf-t2);
				}
			};
			auto B = [=](double t){
				if(0<=t && t<t1){
					return B0-(B0-B1)*(t/t1);
				}
				else if(t1<=t && t<t2){
					return B1;
				}
				else{
					return B1 + (B0-B1)*(t-t2)/(tf-t2);
				}
			};

			for(double t=0; t<tf; t+=dt){
				std::cout << "tf= " << tf << " t = " << t << std::endl;
				timeevolve(mps, J, h, G, A, B, t, dt, {"Cutoff", 1E-8});
			}

			myvector<ITensor> op_list(size);

			for(size_t i=1; i<=size; i++){
				op_list[i] = id(mps.site_c(i));
			}

			double Pgs = sqrt(norm(overlap(init_mps, op_list, mps))); 
			double Pgs2 = sqrt(norm(overlap(init_mps_rev, op_list, mps))); 
			std::cout << "Pgs: " << Pgs << std::endl;
			std::cout << "Pgs2: " << Pgs2 << std::endl;
			ofs << tf << " " << (-1./size)*log(Pgs+Pgs2) << "\n";
			ofs.flush();
		}
	}

	return 0;
}
